helpers do
  def current_user
    User.get(session[:id]) || GuestUser.new
  end

  def authenticate!
    unless current_user.registered?
      redirect '/auth/cas'
    end
  end

  def logout!
    session[:id] = nil
  end
end
