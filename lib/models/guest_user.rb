class GuestUser
  def name
    'Gastuser'
  end

  def may_edit?(o)
    false
  end

  def registered?
    false
  end
end
