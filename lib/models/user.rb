class User
  include DataMapper::Resource
  property  :id,       Serial
  property  :uid,      String, unique: true
  property  :name,     String
  property  :email,    String, unique: true
  property  :is_admin, Boolean

  def to_s
    name || uid
  end

  def may_edit?(o)
    o.user == self || is_admin
  end

  def registered?
    true
  end
end
