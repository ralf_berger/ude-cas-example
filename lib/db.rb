# coding: utf-8
DataMapper.setup(:default, ENV['DATABASE_URL'] || "sqlite3://#{Dir.pwd}/db.sqlite3")

require './lib/models/user'
require './lib/models/guest_user'

DataMapper.finalize.auto_upgrade!
