#!/usr/bin/env ruby
# coding: utf-8
require 'bundler'
Bundler.require
require './lib/db'
require './lib/helpers'

enable :sessions

require 'net/https'
use OmniAuth::Builder do
  provider :cas,
    host:                     'cas.uni-duisburg-essen.de',
    login_url:                '/cas/login',
    service_validate_url:     '/cas/serviceValidate',
    path_prefix:              '/auth',
    disable_ssl_verification: true
end

get '/' do
  haml '%h2 Startseite'
end

get '/protected' do
  authenticate!
  haml '%h2 Mitgliederbereich'
end

get '/auth/cas/callback' do
  begin
    user = User.first_or_create uid: env['omniauth.auth']['uid']
    session[:id] = user.id
    haml "%h2 Angemeldet\n%a{href: '/'} okay"
  rescue
    redirect '/auth/failure'
  end
end

get '/auth/failure' do
  logout!
  haml '%h2 Anmeldefehler'
end

get '/auth/logout' do
  logout!
  haml "%h2 Abgemeldet\n%a{href: '/'} okay"
end
